```
sudo apt install libgecode-dev
g++ -O3 -fPIC -lgecodedriver -lgecodesupport -lgecodekernel -lgecodesearch -lgecodeint -lgecodeminimodel -lgecodegist -lgecodeset -lpthread stripes.cc -o stripes `pkg-config Qt5Widgets --libs` `pkg-config Qt5Widgets --cflags`
```

To see help, run `./stripes -help`.  The options relevant to stripes
are `-dirs`, `-stripes`, `-required` and `-ecc`.  You may also want to
use `-threads 0`.
