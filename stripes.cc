#include <gecode/driver.hh>
#include <gecode/int.hh>
#include <gecode/set.hh>
#include <gecode/minimodel.hh>

using namespace Gecode;

class DistributedPartitionOptions : public Options {
private:
  Driver::UnsignedIntOption _dirs;
  Driver::UnsignedIntOption _stripes;
  Driver::UnsignedIntOption _required;
  Driver::BoolOption _ecc;
public:
  DistributedPartitionOptions(const char* s, unsigned int dirs0,
			      unsigned int stripes0, unsigned int required0, bool ecc0)
    : Options(s),
      _dirs("dirs", "number of dirs to distribute to", dirs0),
      _stripes("stripes", "number of stripes to chunk files into", stripes0),
      _required("required", "how many dirs are needed for data restore", required0),
      _ecc("ecc", "require error correction bit", ecc0) {
    add(_dirs); add(_stripes); add(_required); add(_ecc);
  }

  unsigned int dirs(void) const { return _dirs.value(); }
  unsigned int stripes(void) const { return _stripes.value(); }
  unsigned int required(void) const { return _required.value(); }
  bool ecc(void) const { return _ecc.value(); }
};

class DistributedPartition : public Script {
private:
  const DistributedPartitionOptions &_opt;

  void recurse(SetVar &var, int i, int r) {
    if (--r > 1) {
      for (int j = i+1; j < dist.size(); ++j) {
	SetVar var2(*this);
	rel(*this, dist[j], SOT_UNION, var, SRT_EQ, var2);
	rel(*this, var2 != IntSet(1,_opt.stripes()));
	recurse(var2, j, r);
      }
    } else {
      for (int j = i+1; j < dist.size(); ++j) {
	SetVar var2(*this);
	// n-r-1 sets may not form the whole set
	rel(*this, dist[j], SOT_UNION, var, SRT_EQ, var2);
	rel(*this, var2 != IntSet(1,_opt.stripes()));
	for (int k = j+1; k < dist.size(); ++k) {
	  // n-r sets must form the whole set
	  rel(*this, dist[k], SOT_UNION, var2, SRT_EQ, IntSet(1, _opt.stripes()));
	}
      }
    }
  }

public:
  SetVarArray dist;
  DistributedPartition(const DistributedPartitionOptions& opt)
    : Script(opt), _opt(opt),
      dist(*this,opt.dirs(),IntSet::empty,IntSet(1,opt.stripes())) {
    // Symmetry breaking
    rel(*this, min(dist[0]) >= 2);
    convex(*this, dist[0]);
    rel(*this, max(dist[1]) < opt.stripes());
    rel(*this, min(dist[2]) == 1);
    if (dist.size() > 3)
      rel(*this, max(dist[3]) == opt.stripes());
    // Cardinality needs to match
    IntVar partSize(*this, 0, opt.stripes());
    for (int i = 0; i < dist.size(); ++i) {
      rel(*this, partSize == cardinality(dist[i]));
      // Limits on partition set size
      cardinality(*this, dist[i], 2, opt.stripes()-2);
      for (int j = i+1; j < dist.size(); ++j) {
	// Disjoint
	rel(*this, dist[i], SRT_NQ, dist[j]);
      }
      recurse(dist[i], i, opt.required()-1);
    }

    if (opt.ecc()) {
      for (int i = 1; i <= opt.stripes(); ++i) {
	BoolVarArray hits(*this, dist.size());
	for (int j = 0; j < dist.size(); ++j) {
	  hits[j] = expr(*this, (dist[j] & IntSet(i,i)) == IntSet(i,i));
	}
	// Losing any two directories still leaves three copies
	rel(*this, sum(hits) >= 5);
      }
    }
    branch(*this, dist, SET_VAR_SIZE_MIN(), SET_VAL_MED_INC());
  }

  virtual void print(std::ostream& os) const {
    for (int i = 0; i < dist.size(); ++i) {
      os << i << ": ";
      for (SetVarGlbValues d(dist[i]); d(); ++d) {
	os << d.val() << " ";
      }
      os << std::endl;
    }
  }

  DistributedPartition(DistributedPartition& s)
    : Script(s), _opt(s._opt) {
    dist.update(*this, s.dist);
  }

  virtual Space *copy(void) {
    return new DistributedPartition(*this);
  }
};
  
int main(int argc, char *argv[]) {
  DistributedPartitionOptions opt("DistributedPartition", 5, 10, 3, false);
  opt.parse(argc, argv);
  // TODO sanity checking, not all parameters make sense
  Script::run<DistributedPartition,DFS,DistributedPartitionOptions>(opt);
  return 0;
}
